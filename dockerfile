FROM node:17-alpine3.14
WORKDIR /app
EXPOSE 4000
COPY package.json package-lock.json /app/
RUN  npm ci --silent
COPY ./src /app/src
CMD npm start