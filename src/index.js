import express from 'express'
const app = express()
app.get('/health', (_, res) => {
  res.json({'healthcheck':'alive'})
})
app.listen(4000, () => console.log('server running at port 4000'))

